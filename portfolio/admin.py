from django.contrib import admin
from mezzanine.pages.admin import PageAdmin
from mezzanine.core.admin import TabularDynamicInlineAdmin
from .models import Project, ProjectFile


class ProjectFileInline(TabularDynamicInlineAdmin):
    model = ProjectFile


class ProjectLibraryAdmin(PageAdmin):
    inlines = (ProjectFileInline,)

admin.site.register(ProjectFile)
admin.site.register(Project, ProjectLibraryAdmin)