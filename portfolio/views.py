__author__ = 'christopherfricke'
from calendar import month_name
from collections import defaultdict

from django.http import Http404
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404
from django import VERSION

from mezzanine.conf import settings
from mezzanine.generic.models import AssignedKeyword, Keyword
from mezzanine.utils.views import render, paginate

from portfolio.models import Project


from django.http import HttpResponse


def portfolio_list(request, tag=None, year=None, month=None,
                   username=None, category=None, template="pages/project_list.html"):
    """
    Display a list of portfolio projects that are filtered by tag, year, month, or category.
    """
    settings.use_editable()
    templates = []
    projects = Project.objects.published(for_user=request.user)

    portfolio = {}
    for project in projects:
        try:
            portfolio[project.date_created.strftime('%Y')].append(project)

        except KeyError:
            portfolio[project.date_created.strftime('%Y')] = [project]

    """
    projects = paginate(projects,
                        request.GET.get("page", 1),
                        10,
                        10)
    """
    context = {
        "portfolio": portfolio,
        "year": year,
        "month": month,
        "tag": tag,
        "category": category
    }
    templates.append(template)

    return render(request, templates, context)