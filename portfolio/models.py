from django.db import models
from django.utils.translation import ugettext_lazy as _
from mezzanine.core.models import RichText
from mezzanine.pages.models import Page


class ProjectFile(models.Model):
    """ A model for Portfolio pictures, pdfs or documents
    """
    title = models.CharField(max_length=50)
    file = models.FileField(upload_to="portfolio")
    thumbnail = models.ImageField(upload_to="portfolio")
    primary = models.BooleanField()
    project_files = models.ForeignKey("Project", related_name="project_files")

    class Meta:
        verbose_name = _("Project File")
        verbose_name_plural = _("Project Files")

    def __unicode__(self):
        return self.title


class Project(Page, RichText):
    """ A Singular Project
    """
    date_created = models.DateField("Date Created")

    class Meta:
        verbose_name = _("Project")
        verbose_name_plural = _("Projects")

    def __unicode__(self):
        return self.title

    def keyword_list(self):
        return getattr(self, "_keywords", self.keywords.all())
