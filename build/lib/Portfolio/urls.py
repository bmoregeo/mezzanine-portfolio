__author__ = 'christopherfricke'

from django.conf.urls.defaults import patterns, url

from mezzanine.conf import settings

# Leading and trailing slahes for urlpatterns based on setup.
_slashes = (
    "/" if settings.BLOG_SLUG else "",
    "/" if settings.APPEND_SLASH else "",
)

urlpatterns = patterns("portfolio.views",
                       url(r"^$", "portfolio_list")
)
