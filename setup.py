from distutils.core import setup

setup(
    name='portfolio',
    version='0.9',
    packages=['portfolio'],
    url='',
    license='',
    author='christopherfricke',
    author_email='christopher.fricke@bmoregeo.com',
    description='',
    include_package_data=True
)
